/*
Buatkan query untuk menampilkan data employee yang di hire/tanggal mulai kerjanya di bulan agustus.
Simpan querynya di dalam file summative4.sql
*/
SELECT employee_id as "employee id", concat(first_name," ", last_name) as name, email, phone_number as "phone number",
	   hire_date as "hire date", job_id as "job id", salary, commission_pct as "commission", manager_id as "manager id", department_id as "department id"
FROM employee
WHERE MONTH(hire_date) = 8;