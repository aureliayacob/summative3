/*
Tampilkan data semua student & nama lesson/pelajaran dan nilai untuk masing-masing pelajarannya.. 
Simpan querynya sebagai sumative2.sql
*/

SELECT s.`name` as Student, l.`name` as Lesson, sc.`score` as Score
FROM student s
JOIN score sc
ON s.studentId = sc.studentId
JOIN lesson l
ON sc.lessonId= l.lessonId;