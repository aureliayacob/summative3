/*
Buat database dengan nama summativedb lalu buat 3 table seperti yang digambarkan di bawah ini:
*/
CREATE DATABASE summativedb;
CREATE TABLE summativedb.Student (
`studentId` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL,
`surname` VARCHAR(45),
`birthDate` VARCHAR(30),
`gender` VARCHAR (20),
PRIMARY KEY (`studentId`));

CREATE TABLE summativedb.Lesson(
`lessonId` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL,
`level` VARCHAR(45),
PRIMARY KEY (`lessonId`));

CREATE TABLE summativedb.Score(
`scoreId` INT NOT NULL AUTO_INCREMENT,
`studentId` INT,
`lessonId` INT,
`score` INT,
PRIMARY KEY(`scoreId`),
FOREIGN KEY (`studentId`) REFERENCES `student`(`studentId`),
FOREIGN KEY (`lessonId`) REFERENCES `lesson`(`lessonId`));


/*Buat data samples untuk table Student, Lesson & score ujian mereka di masing-masing table
Simpan query insert & create table saat pembuatan table yang dilakukan di slide sebelumnya

Simpan di dalam file summative1.sql
*/

INSERT INTO student (`name`, `surname`,`birthDate`, `gender`)
VALUES ("Ayuna", "Armonica", "29-03-2000", "Female"),
	   ("Ikhsanudin", "Nugraha", "03-10-1999", "Male"),
	   ("Felix", "Prihantoro", "02-12-1998", "Male"),
	   ("Pipo", "Aja", "05-06-2000", "Male"),					
	   ("Desha", "Purnomo", "17-12-1998", "Female");   
select * from student;
INSERT INTO lesson (`name`, `level`)
VALUES ("Delay Tolerant Network", "Level 7"),
	   ("Kriptografi", "Level 8"),
	   ("Simulator", "Level 7"),       
	   ("Mobile Network", "Level 6"),
	   ("Query Language", "Level 4");
select * from lesson;
INSERT INTO score (`studentId`, `lessonId`, `score`)
VALUES (1,1, 90), (1,2,80), (1,3,95), (1,4,85), (1,5,100),  
	   (2,1, 70), (2,2,90), (1,3,65), (1,4,95), (1,5,98),
	   (3,1, 89), (3,2,98), (3,3,100), (3,4,99), (1,5,100),       
	   (4,1, 50), (4,2,100), (4,3,98), (4,4,89), (4,5,90),
	   (5,1, 96), (5,2,85), (5,3,65), (5,4,94), (5,5,76);
select * from score;
