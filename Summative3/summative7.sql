/*
Tulis query untuk mendapatkan departement ID, nama departement, tahun, dan jumlah karyawan yang bergabung.
Simpan querynya sebagai summative7.sql
*/

SELECT d.department_id as "Department id", d.department_name as "Department name", 
YEAR(e.hire_date) as "Year", count(employee_id) as "Number of employees"
FROM departement d
JOIN employee e
ON d.department_id = e.department_id
GROUP BY d.department_id;