/*
Buka file summative2-1.txt, buat table dengan nama “departement” dan isi datanya yang menyerupai dengan data yang ada di file summative2-1.txt.
Simpan querynya sebagai summative6.sql
*/

CREATE TABLE summativedb.departement (
`DEPARTMENT_ID` INT NOT NULL AUTO_INCREMENT, 
`DEPARTMENT_NAME` VARCHAR(45),
`MANAGER_ID` INT,
`LOCATION_ID` INT,
PRIMARY KEY(`DEPARTMENT_ID`));

ALTER TABLE employee ADD FOREIGN KEY (`DEPARTMENT_ID`) REFERENCES `departement`(`DEPARTMENT_ID`);

INSERT INTO departement 
VALUES (10  , 'Administration' ,200  ,1700 ),
(20  , 'Marketing',201  ,1800  ),
(30  , 'Purchasing',114  ,1700  ),
(40  ,'Human Resources',203  ,2400  ),
(50  , 'Shipping',121  ,1500  ),
(60  , 'IT',103  ,1400  ),
(70  , 'Public Relations',204  ,2700  ),
(80  , 'Sales',145  ,2500  ),
(90  , 'Executive',100  ,1700  ),
(100  , 'Finance',108  ,1700  ),
(110  , 'Accounting',205  ,1700  ),
(120  , 'Treasury',0  ,1700  ),
(130  , 'Corporate Tax',0  ,1700  ),
(140  , 'Control And Credit',0  ,1700  ),
(150  , 'Shareholder Services',0  ,1700  ),
(160  , 'Benefits',0  ,1700  ),
(170  , 'Manufacturing',0  ,1700  ),
(180  , 'Construction',0  ,1700  ),
(190  , 'Contracting',0  ,1700  ),
(200  , 'Operations',0  ,1700  ),
(210  , 'IT Support',0  ,1700  ),
(220  , 'NOC',0  ,1700  ),
(230  , 'IT Helpdesk',0  ,1700  ),
(240  , 'Government Sales',0  ,1700  ),
(250  , 'Retail Sales',0  ,1700  ),
(260  , 'Recruiting',0  ,1700  ),
(270  , 'Payroll',0  ,1700  );