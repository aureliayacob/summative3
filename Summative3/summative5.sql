/*
Buatkan query untuk menampilkan data employee yang di hire/tanggal mulai di antara tanggal 1 juni 1987 & 30 juli 1987
Simpan querynya di file summative5.sql
*/

SELECT employee_id as "Employee id", concat(first_name," ", last_name) as Name, email as Email, phone_number as "Phone number",
	   hire_date as "Hire date", job_id as "Job id", salary as Salary, commission_pct as "Commission", manager_id as "Manager id", 
       department_id as "Department id"
FROM employee
WHERE hire_date BETWEEN '1987-06-01' and '1987-07-30';